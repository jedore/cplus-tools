/*
 * Author: Jedore Cui
 * Date: 2019/08/14
 * Description: varargs for log
 */

#include <iostream>
#include <cstdarg>
#include <ctime>

void log(const char *format, ...) {
    char buf[1024];
    va_list ap;
    va_start(ap, format);
    vsprintf(buf, format, ap);
    // add some common info e.g. time
    time_t now;
    time(&now);
    char tmp[64];
    strftime(tmp, sizeof(tmp), "[%Y-%m-%d %H:%M:%S] ", localtime(&now));

    printf("%s %s\n", tmp, buf);
}


int main() {
    std::string a = "test";
    int b = 9999;
    log("%s main %d", a.c_str(), b);
    return 0;
}

